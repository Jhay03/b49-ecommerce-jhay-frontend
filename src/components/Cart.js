import React, { useState, useEffect } from 'react';
import Swal from "sweetalert2"
import Stripe from "./forms/Stripe"
import {URL} from "../config"
const Cart = ({ token, user, products }) => {
    const [cartItems, setCartItems] = useState([])
    let total = 0;
    useEffect(() => {
        setCartItems(JSON.parse(localStorage.getItem('cartItems')))
    }, [])

    if (cartItems.length) {
        let subTotals = cartItems.map(item => parseInt(item.price) * parseInt(item.quantity))
        //array or prices
    
         total = subTotals.reduce((accumulator, subPerItem) => {
            return accumulator + subPerItem
        })
    }
   
    const emptyCartHandler = () => {
        localStorage.removeItem("cartItems")
        let cartItems = []
        localStorage.setItem("cartItems", JSON.stringify(cartItems))
        window.location.href="/cart"
    }


    //solution ng instructor ko
    const deleteCartItem = (productId) => {
        let updatedCart = cartItems.filter(item => item._id !== productId)
        localStorage.setItem('cartItems', JSON.stringify(updatedCart))
        setCartItems(JSON.parse(localStorage.getItem('cartItems')))
    }
    
    //solution ko
   /* const deletePerItem = () => {
        let counter = []
        let index = counter.indexOf(0)
        cartItems.splice(index, 1)
        localStorage.setItem('cartItems', JSON.stringify(cartItems))
    }*/

    const onChangeHandler = (quantity, productId) => {
        //alert("Quantity: " + quantity + " - " + productId)
        let itemToUpdate = cartItems.find(item => item._id === productId)
        itemToUpdate.quantity = parseInt(quantity)
        let updatedCart = cartItems.map(item => { 
            return item._id === productId ? 
             {...item, ...itemToUpdate}   
            : item
        })
        localStorage.setItem('cartItems', JSON.stringify(updatedCart))
    }
  

    const checkOut = () => {
        let orders = JSON.parse(localStorage.getItem('cartItems'))
        fetch(`${URL}/transactions`, {
            method: "POST",
            body: JSON.stringify({ orders, total }),
            headers: {
                "Content-Type": "application/json",
                "x-auth-token": localStorage.getItem('token')
            }
        })
            .then(res => res.json())
            .then(data => {
            //console.log(data)
                Swal.fire({
                    'icon': 'success',
                    'title': data.message
                })
                localStorage.setItem('cartItems', JSON.stringify([]))
                setCartItems(JSON.parse(localStorage.getItem('cartItems')))
        })
    }
    return (
        <div className="col-md-10 mx-auto container">
            <h2> My Cart </h2>
            {
                cartItems.length ?
                    <table className="table table-hover">
                        <thead className="thead-dark">
                            <tr>
                                <th>Item</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Subtotal</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {cartItems.map(item => (
                                <tr key={item._id}>
                                    <td>{item.name}</td>
                                    <td>{item.price}</td>
                                    <td><input type="number" name="quantity"
                                        value={item.quantity}
                                        min="1"
                                        onChange={(e) => onChangeHandler(e.target.value, item._id)} /></td> 
                                    <td>{item.quantity * item.price}</td>
                                    <td>
                                       {/*<button className="btn btn-outline-danger mr-2" onClick={deletePerItem}>Delete</button> */ } 
                                        <button className="btn btn-outline-danger mr-2"
                                            onClick={() => deleteCartItem(item._id)}>
                                            Delete</button>
                                    </td>
                                </tr>
                            ))}
                            <tr>
                                <td colSpan="3">Total</td>
                                <td colSpan="2">PHP : {total}</td>
                            </tr>
                            <tr>
                                <td colSpan="5" className="text-center">
                                    <button className="btn btn-outline-danger mr-2" onClick={emptyCartHandler}>Empty Cart</button>
                                    <button className="btn btn-outline-success mr-2" onClick={checkOut}>Check Out</button>
                                    <Stripe amount={total * 100} cartItems={cartItems} setCartItems={setCartItems}/>
                                </td>
                            </tr>
                            </tbody>
                    </table>
                    :
                    <h3 className="text-center">Cart is Empty</h3>
            }
        </div>
    )
}

export default Cart;