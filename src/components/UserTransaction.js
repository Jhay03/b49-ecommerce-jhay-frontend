import React, {useState, useEffect} from 'react';
import { useParams } from 'react-router-dom'
import {URL} from "../config"
const UserTransaction = () => {
    let {userId} = useParams()
    const [transactions, setTransactions] = useState([])
    useEffect(() => {
        fetch(`${URL}/transactions/${userId}`, {
            headers: {
                "x-auth-token": localStorage.getItem('token')
            }
        })
            .then(res => res.json())
        .then(data => setTransactions(data))
    })

   
    return (
        <div className="container">
            <h1 className="text-center">My Transactions </h1> 
            {
                transactions.length ? 
            <table className="table table-hover">
                <thead className="thead-dark">
                        <tr>
                            <th>Transaction ID</th>
                            <th>Status</th>
                            <th>Amount</th>
                            <th>Date</th>
                        </tr>   
                </thead>
                        <tbody>
                            {transactions.map(transaction => (
                                <tr key={transaction._id}>
                                    <td>{transaction._id}</td>
                                        <td>
                                        {   transaction.statusId == "5e93c94dd1cfb0544d638c3e" ?
                                            "Pending" : (transaction.statusId == "5e93c93dd1cfb0544d638c2f" ?
                                            "Completed" : "Cancelled"
                                            )
                                        }
                                    </td>
                                    <td>PHP : {transaction.total}</td>
                                    <td>{new Date(transaction.dateCreated).toLocaleString()}</td>
                                </tr>
                                    ))}
                        </tbody>
            </table>
                         :<h2 className="text-center"> No transactions to show. </h2>}
        </div>
    )
}

export default UserTransaction;