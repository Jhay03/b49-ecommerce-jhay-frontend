import React, {useState} from 'react';
import {
    Container, Col, Card, CardTitle,
    Row, CardText, Button, CardImg
} from "reactstrap";
import { Link } from 'react-router-dom';
import { URL } from "../config"

const Catalog = ({ products, user, token }) => {
    const style = {
        "height": "50vh",
        "width": "100%"
    }
    const [quantity, setQuantity] = useState(0)
    const addToCartHanlder = (e, product) => {
       // alert(e.target.previousElementSibling.value)
        let cartItems = JSON.parse(localStorage.getItem('cartItems'))
        if (cartItems.length) {
            let matched = cartItems.find(item => {
               return item._id === product._id
            })
            if (matched) {
                let updatedCart = cartItems.map(item => {
                    return item._id === product._id ?
                        { ...item, quantity: item.quantity += parseInt(e.target.previousElementSibling.value) } :
                        item
                })
                localStorage.setItem('cartItems', JSON.stringify(updatedCart))
            } else {
                cartItems.push({...product, quantity})
            }
        } else {
            cartItems.push({...product, quantity})
        }
        localStorage.setItem('cartItems', JSON.stringify(cartItems))
    }
    const showProducts = products.map( product => (
        <Col sm="4" key={product._id}> 
            <Card className="p-2 mb-4">
                <CardImg src={`${URL}${product.image}`} style={style}/>
                <Link to={`/products/${product._id}`}>
                    <CardTitle className="text-center">Name: {product.name}</CardTitle>
                </Link>
                <CardText>Description: {product.description}</CardText>
                <h6>Price: {product.price}</h6>
                {user && token && user.isAdmin === false ? 
                    <div>
                        <input type="number" min="1" className="form-control"
                            onChange={(e) => setQuantity(parseInt(e.target.value))} />
                        <Button className="mt-2" onClick={(e) => {
                            addToCartHanlder(e, product)
                            e.target.previousElementSibling.value = ''
                    }}>Add To Cart!</Button>
                </div> : null
                }
            </Card>
        </Col>
    ))

    return (
        <Container>
            <h2> All Products</h2>
            <Row>
                {showProducts}
           </Row>
       </Container>
    )
}

export default Catalog;