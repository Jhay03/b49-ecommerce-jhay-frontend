import React, {useState, useEffect} from 'react';
import Swal from "sweetalert2";
import {URL } from "../../config"

const AddProduct = () => {
    const [formData, setFormData] = useState({})
    const [categories, setCategories] = useState([])
    useEffect(() => {
        fetch(`${URL}/categories`, {
            headers: {
                "x-auth-token": localStorage.getItem("token")
            }
        })
            .then(res => res.json())
            .then(data => {
                setCategories(data)
        })
    }, [])

    const onChangeHandler = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
    }

    const handleFile = (e) => {
        setFormData({
            ...formData,
            image: e.target.files[0]
        })
    }
    const onSubmit = (e) => {
        e.preventDefault()
        const product = new FormData()
        product.append('name', formData.name)
        product.append('description', formData.description)
        product.append('categoryId', formData.categoryId)
        product.append('price', formData.price)
        product.append('image', formData.image)
        //alert("Hello!")
        fetch(`${URL}/products`, {
            method: "POST",
            body: product,
            headers: {
                "x-auth-token": localStorage.getItem("token")
            }
        })
            .then(res => res.json())
            .then(data => {
                if (data.status === 200) {
                    Swal.fire({
                        icon: "success",
                        text: data.message
                    })
                    window.location.href="/"
                } else {
                    Swal.fire({
                    icon: "error",
                    text: "Invalid inputs"
                    })
                }
        })
    }
    return (
        <form className="mx-auto col-sm-6" onSubmit={onSubmit} encType="multipart/form-data">
            <h1 className="text-center" >Add a Product </h1>
            
                <div className="form-group">
                    <label htmlFor="name"> Name </label>
                    <input type="text"
                        name="name"
                        id="name"
                        className="form-control"
                        onChange={onChangeHandler}
                    />
                </div>

                <div className="form-group">
                    <label htmlFor="description"> Description </label>
                    <input type="text"
                        name="description"
                        id="description"
                        className="form-control"
                        onChange={onChangeHandler}
                    />
                </div>

                <div className="form-group">
                    <label htmlFor="price"> Price </label>
                    <input type="number"
                        min="1"
                        name="price"
                        id="price"
                        className="form-control"
                    onChange={onChangeHandler}
                        
                    />
                </div>

                <div className="form-group">
                    <label htmlFor="image"> Image </label>
                    <input type="file"
                        name="image"
                        id="image"
                        className="form-control"
                        onChange={handleFile}
                    />
            </div>
                  <select className="form-control" name="categoryId" onChange={onChangeHandler}>
                    <option disabled selected>Select Category</option>
                    {categories.map(category => (
                        <option value={category._id} key={category._id}>{category.name}</option>
                    ))}
                </select>
                <button className="btn btn-outline-primary mt-2">Add</button>
        </form>
    )
}

export default AddProduct;