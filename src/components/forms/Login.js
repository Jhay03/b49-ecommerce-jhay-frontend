import React, { useState } from 'react';
import Swal from "sweetalert2";
import { URL } from "../../config"
const Login = () => {
    const [formData, setFormData] = useState([])
    
    const onChangeHandler = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })    
    }
    const onSubmitHandler = (e) => {
        e.preventDefault() // to prevent reloading of the page
        //alert(JSON.stringify(formData))

        let reqOptions = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type" : "application/json"
            }
        }
        fetch(`${URL}/users/login`, reqOptions)
            .then(res => res.json())
            .then(data => {
                if (data.auth) {
                    Swal.fire({
                        icon: "success",
                        title: data.message,
                        showConfirmButton: false,
                        timer: 4500
                    })
                    localStorage.setItem('user', JSON.stringify(data.user))
                    localStorage.setItem('token', data.token)
                    let cartItems = []
                    localStorage.setItem('cartItems', JSON.stringify(cartItems))
                    window.location.href="/"
                } else {
                    Swal.fire({
                        icon: "error",
                        title: data.message,
                        showConfirmButton: false,
                        timer: 1500
                    })
            }
        })
    }
    return (
        <form className="container col-sm-6 mx-auto" onSubmit={onSubmitHandler}>
            <h1 className="text-center">Login Form</h1>
            <div className="form-group">
                <label htmlFor="username">Username</label>
                <input
                    type="text"
                    name="username"
                    id="username"
                    className="form-control"
                    onChange={onChangeHandler}
                    
                />
            </div>
            <div className="form-group">
                <label htmlFor="password">Password</label>
                <input
                    type="password"
                    name="password"
                    id="password"
                    className="form-control"
                    onChange={onChangeHandler}
                />
            </div>
            <button className="btn btn-outline-success">Login</button>
        </form>
    )
}

export default Login;