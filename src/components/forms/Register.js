import React, {useState} from 'react';
import Swal from "sweetalert2";
import { URL } from "../../config"
const Register = () => {
    const [formData, setFormData] = useState({})
    const onChangeHanlder = (e) => {
        setFormData({
            ...formData,
            [e.target.name] : e.target.value
        })
    }

    const onSubmitHandler = (e) => {
        e.preventDefault()
        //console.log(formData)
        fetch(`${URL}/users`, {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => res.json())
            .then(data => {
                if (data.status === 200) {
                    Swal.fire({
                        icon: 'success',
                        text: data.message
                    })
                } else {
                    Swal.fire({
                        icon: 'error',
                        text: data.message,
                        timer: 1500,
                        showConfirmButton: false
                    })
                }
            })
    }
    return (
        <form className="container col-sm-6 mx-auto" onSubmit={onSubmitHandler}>
            <div className="form-group">
                <h1 className="text-center">Registration Form</h1>
                <label htmlFor="fullname"> Fullname</label>
                <input
                    type="text"
                    name="fullname"
                    id="fullname"
                    className="form-control"
                    onChange={onChangeHanlder}
                />
            </div>

            <div className="form-group">
                <label htmlFor="username"> Username</label>
                <input
                    type="text"
                    name="username"
                    id="username"
                    className="form-control"
                    onChange={onChangeHanlder}
                />
            </div>

            <div className="form-group">
                <label htmlFor="password"> Password</label>
                <input
                    type="password"
                    name="password"
                    id="password"
                    className="form-control"
                    onChange={onChangeHanlder}
                />
            </div>
            
            <div className="form-group">
                <label htmlFor="password2"> Confirm Password</label>
                <input
                    type="password"
                    name="password2"
                    id="password2"
                    className="form-control"
                    onChange={onChangeHanlder}
                />
            </div>
            <button className="btn btn-outline-warning"
            >Submit</button>
            </form>
    )
}

export default Register;